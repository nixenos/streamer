import pyaudio
import socket
import select

class audioServer:
    # PYAUDIO CONFIG
    # AUDIOFORMAT = pyaudio.paInt16
    # CHANNELS = 1
    # BITRATE = 44100
    # CHUNK_SIZE = 1024

    # # SERVER CONFIG
    # SERVER_IP = "127.0.0.1"
    # SERVER_PORT = 6166

    # PYAUDIO INITIALIZE

    AUDIOSTREAM : pyaudio.PyAudio
    MAINSOCKET : socket.socket
    AUDIOFORMAT : pyaudio.paInt16
    CHANNELS : int
    BITRATE : int
    CHUNKSIZE : int
    SERVERIP : str
    SERVERPORT : int
    connectionList : List[socket.socket]

    def __init__(self, audioFormat, channelsCount, bitRate, chunkSize, serverIP, serverPort):
        self.set_audio_properites(audioFormat, channelsCount, bitRate, chunkSize)
        self.set_network_conf(serverIP, serverPort)
        print("Custom configuration loaded")

    def __init__(self):
        self.set_audio_properites(pyaudio.paInt16, 1, 44100, 1024)
        self.set_network_conf("localhost", 6166)


    def audioSocketInit(self):
        self.audioStream = pyaudio.PyAudio()


    def set_network_conf(self, serverIP, serverPort):
        self.SERVERIP = serverIP
        self.SERVERPORT = serverPort


    def get_network_conf(self):
        return (self.serverIP, self.serverPort)


    def set_audio_properites(self, audioFormat, channels, bitRate, chunkSize):
        self.AUDIOFORMAT = audioFormat
        self.CHANNELS = channels
        self.BITRATE = bitRate
        self.CHUNKSIZE = chunkSize


    def get_audio_properties(self):
        return (self.AUDIOFORMAT, self.CHANNELS, self.BITRATE, self.CHUNKSIZE)


    def estabilish_connection(self):
        # CONNECTION SETTING
        self.mainSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.mainSocket.bind((self.SERVERIP, self.SERVERPORT))
        self.mainSocket.listen(300)


    # CALLBACK FUNCTION
    def callback(in_data, frame_ount, time_info, status):
        for receiver in connectionList[1:]:
            receiver.send(in_data)
        return (None, pyaudio.paContinue)

    # connection list init

    connectionList = [mainSocket]
    # MICROPHONE INIT
    microphoneStream = audioStream.open(format=AUDIOFORMAT, channels=CHANNELS, rate=BITRATE, input=True, frames_per_buffer=CHUNK_SIZE, stream_callback=callback)

    # PROMPT TO START RECORDING

    # print("If you want to start streaming, input Yes and press enter: ")
    inp = input("If you want to start streaming, input Yes and press enter: ")

    if inp == "Yes":
        microphoneStream.start_stream()
    else:
        quit()

    print("Streaming started...")

    try:
        while 1:
            readable, writeable, errored  = select.select(connectionList, [], [])
            for receiver in readable:
                if receiver is mainSocket:
                    (receiverSocket, adress) = mainSocket.accept()
                    connectionList.append(receiverSocket)
                    print("New connection from adress: " + str(adress))
                else:
                    data = receiver.recv(1024)
                    if not data:
                        connectionList.remove(receiver)
    except KeyboardInterrupt:
        pass

    print("Streaming finished!")

    # CLOSE ALL
    mainSocket.close()
    microphoneStream.stop_stream()
    microphoneStream.close()
    audioStream.terminate()