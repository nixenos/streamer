# streamer

Simple utility to stream audio over network using python.

## Dependencies

- pyAudio (installed via package manager, not pip, as it's not compiling)

It has some lag, because of buffering audio before outputing it to media stream

## Credits:
https://gist.github.com/fopina

## HOW-TO TESTING

1. Clone repository
2. Install pyAudio from your package manager (pip package is broken for python ver. >3.6)
3. If you want to test it over network change SERVER_IP in both client.py and server.py to your server's IP (default is localhost)
4. Run ```python3 server.py```, when asked to start streaming write 'Yes' to console and hit enter
5. Open client with command ```python3 client.py```
6. If you want to stop client, hit ctrl+c, same for server

