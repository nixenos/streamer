import pyaudio
import socket, sys, queue

AUDIO_FORMAT = pyaudio.paInt16
AUDIO_CHANNELS = 1
BITRATE = 44100
CHUNK_SIZE = 1024

SERVER_IP = "127.0.0.1"
SERVER_PORT = 6166

BUFFERSIZE = 5

mainSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mainSocket.connect((SERVER_IP, SERVER_PORT))

audio = pyaudio.PyAudio()
mainStream = audio.open(format=AUDIO_FORMAT, channels=AUDIO_CHANNELS, rate=BITRATE, output=True, frames_per_buffer=CHUNK_SIZE)

audioBuffer = queue.Queue(maxsize=BUFFERSIZE)

try:
    while 1:
        dataReceived = mainSocket.recv(CHUNK_SIZE)
        if not dataReceived:
            break

        if audioBuffer.qsize() == BUFFERSIZE:
            currentData = audioBuffer.get()
            audioBuffer.put(dataReceived)
            mainStream.write(currentData)
        else:
            audioBuffer.put(dataReceived)
except KeyboardInterrupt:
    pass

print("Exitting...")
mainSocket.close()
mainStream.close()
audio.terminate()